4#!/usr/bin/env python3
# Wrengineers LED project program

import time
from rpi_ws281x import PixelStrip, Color
import argparse

# LED strip configuration:
LED_COUNT = 512        # Number of LED pixels.
LED_PIN = 18          # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN = 10        # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10          # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS =10  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False    # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
WIPE_DELAY=0

palette = [[0, 0, 0], [255, 0, 0], [0, 255, 0], [0, 30, 0], [80, 80, 80], [150, 75, 0], [0, 0, 80], [160,85,68], [0, 0, 255]]

ball = [[ 0, 4, 0,],
        [ 4, 4, 4,],
        [ 0, 4, 0,]]


background = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 1, 3, 0, 1, 0, 0, 0, 0, 1, 0, 0, 3, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 3, 0],
              [1, 3, 3, 3, 1, 3, 3, 3, 3, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
              [1, 3, 3, 3, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
              [3, 3, 3, 3, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]]

post = [[1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1]]

player = [[[0, 0, 5, 0, 0],
           [0, 5, 5, 5, 0],
           [0, 5, 5, 5, 0],
           [0, 0, 5, 0, 0],
           [5, 5, 6, 5, 5],
           [0, 0, 6, 0, 0],
           [0, 0, 6, 0, 0],
           [0, 6, 0, 6, 0],
           [0, 5, 0, 5, 0]],

          [[0, 0, 5, 0, 0],
           [0, 5, 5, 5, 0],
           [0, 5, 5, 5, 0],
           [0, 0, 5, 0, 0],
           [5, 5, 6, 5, 5],
           [0, 0, 6, 0, 0],
           [0, 0, 6, 0, 0],
           [0, 6, 0, 6, 0],
           [5, 0, 0, 5, 0]],

          [[0, 0, 5, 0, 0],
           [0, 5, 5, 5, 0],
           [0, 5, 5, 5, 0],
           [0, 0, 5, 0, 0],
           [5, 5, 6, 5, 5],
           [0, 0, 6, 0, 0],
           [5, 6, 6, 0, 0],
           [0, 0, 0, 6, 0],
           [0, 0, 0, 5, 0]],

          [[0, 0, 5, 0, 0],
           [0, 5, 5, 5, 0],
           [0, 5, 5, 5, 0],
           [5, 0, 5, 0, 5],
           [0, 5, 6, 5, 0],
           [0, 0, 6, 0, 0],
           [0, 0, 6, 0, 0],
           [0, 6, 0, 6, 0],
           [0, 5, 0, 5, 0]]]

Explosion = [[[0, 0, 0, 0, 0, 0, 0, 0, 0,],
              [0, 0, 0, 0, 0, 0, 0, 0, 0,],
              [0, 0, 0, 0, 0, 0, 0, 0, 0,],
              [0, 0, 0, 7, 1, 7, 0, 0, 0,],
              [0, 0, 0, 8, 7, 8, 0, 0, 0,],
              [0, 0, 0, 7, 1, 7, 0, 0, 0,],
              [0, 0, 0, 0, 0, 0, 0, 0, 0,],
              [0, 0, 0, 0, 0, 0, 0, 0, 0,],
              [0, 0, 0, 0, 0, 0, 0, 0, 0,]],

             [[0, 0, 0, 0, 0, 0, 0, 0, 0,],
              [0, 0, 0, 0, 0, 0, 0, 0, 0,],
              [0, 0, 7, 0, 1, 0, 7, 0, 0,],
              [0, 0, 0, 7, 1, 7, 0, 0, 0,],
              [0, 0, 8, 8, 7, 8, 8, 0, 0,],
              [0, 0, 0, 7, 1, 7, 0, 0, 0,],
              [0, 0, 7, 0, 1, 0, 7, 0, 0,],
              [0, 0, 0, 0, 0, 0, 0, 0, 0,],
              [0, 0, 0, 0, 0, 0, 0, 0, 0,]],

             [[0, 0, 0, 0, 0, 0, 0, 0, 0,],
              [0, 7, 0, 0, 1, 0, 0, 7, 0,],
              [0, 0, 7, 0, 1, 0, 7, 0, 0,],
              [0, 0, 0, 7, 1, 7, 0, 0, 0,],
              [0, 8, 8, 8, 7, 8, 8, 8, 0,],
              [0, 0, 0, 7, 1, 7, 0, 0, 0,],
              [0, 0, 7, 0, 1, 0, 7, 0, 0,],
              [0, 7, 0, 0, 1, 0, 0, 7, 0,],
              [0, 0, 0, 0, 0, 0, 0, 0, 0,]],

             [[7, 0, 0, 0, 1, 0, 0, 0, 7,],
              [0, 7, 0, 0, 1, 0, 0, 7, 0,],
              [0, 0, 7, 0, 1, 0, 7, 0, 0,],
              [0, 0, 0, 7, 1, 7, 0, 0, 0,],
              [8, 8, 8, 8, 7, 8, 8, 8, 8,],
              [0, 0, 0, 7, 1, 7, 0, 0, 0,],
              [0, 0, 7, 0, 1, 0, 7, 0, 0,],
              [0, 7, 0, 0, 1, 0, 0, 7, 0,],
              [7, 0, 0, 0, 1, 0, 0, 0, 7,]]]

# Define functions which animate LEDs in various ways.
def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms / 1000.0)

def fastWipe(strip, color, wait_ms=50):
   """Wipe as quickly as possible"""
   for i in range(strip.numPixels()):
       strip.setPixelColor(i, color)
   strip.show()
   time.sleep(wait_ms / 1000.0)

def fastClear(strip):
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(0, 0, 0))

# Set a specific pixel to a color 
def setpixel(strip, x, y, color):
    xfactor=x*16
    if x & 1:
        yfactor=y
    else:
        yfactor=15-y
    strip.setPixelColor(xfactor+yfactor, color)

#  Display a bitmap <bmp> at xpos, ypos on the screen.  <width> and <height> specify the width and height of <bmp>
def displayBitmap(strip, xpos, ypos, width, height, bmp, palette):
    # loop through each row in the bitmap
    for y in range(height):
        # loop through each entry in the row
        for x in range(width):
            # get the pixel color for this specific pixel.  Because row 0 is the top row in the array, we need to 
            # reverse the row number by subtracting it from the height 
            pixel = bmp[height-1-y][x]
            # color 0 is blank
            if pixel != 0:
                color = Color(palette[pixel][0], palette[pixel][1], palette[pixel][2])
                setpixel(strip, xpos+x, ypos+y, color)

def getPlayerX(frame):
    return 24

def getPlayerY(frame):
    if (frame >= 60) and (frame < 70):
        return round((frame - 60) / 2 + 0.1)
    if (frame >= 70) and (frame < 80):
        return round((80 - frame) / 2 + 0.1)
    return 0

def getPlayerFrame(frame):
    if (frame < 20):
        return 0
    if (frame < 24):
        return 1
    if (frame < 32):
        return 2
    if (frame < 36):
        return 1
    if (frame < 60):
        return 0
    if (frame < 80):
        return 3
    return 0

# figure out the X position of the ball for the provided frame number
def getBallX(frame, oldX):
    if (frame < 22):
        return oldX
    if (oldX > 1):
        return oldX - 1
    return oldX

def getBallY(frame):
    if (frame < 22):
        return 0
    if (frame < 36):
        # 36 -> 9, 22 -> 0
        return round((frame - 22)*9/(36-22))
    if (frame < 42):
        # 36 -> 9, 42 -> 0
        return round((42 - frame)*9/(42-36))
    return 0

#    if (frame < 22):
#        return 22 #position before kick
#    if (frame < 42):
#        return 44-frame
#    return 2 #position at the end

def boom(strip, frame, palette):
    if (frame < 42):
        return
    if (frame < 42+26):
        color = Color(palette[7][0], palette[7][1], palette[7][2])
        setpixel(strip, 13, round((frame-42)/2 + 0.1), color)
        setpixel(strip, 23, round((frame-42)/2 + 0.1), color)
        return
    if (frame < 70):
        displayBitmap(strip, 9, 9, 9, 9, Explosion[0], palette)
        displayBitmap(strip, 19, 9, 9, 9, Explosion[0], palette)
        return

    if (frame < 72):
        displayBitmap(strip, 9, 9, 9, 9, Explosion[1], palette)
        displayBitmap(strip, 19, 9, 9, 9, Explosion[1], palette)
        return

    if (frame < 74):
        displayBitmap(strip, 9, 9, 9, 9, Explosion[2], palette)
        displayBitmap(strip, 19, 9, 9, 9, Explosion[2], palette)
        return
 
    if (frame < 76):
        displayBitmap(strip, 9, 9, 9, 9, Explosion[3], palette)
        displayBitmap(strip, 19, 9, 9, 9, Explosion[3], palette)
        return

# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    parser.add_argument('-a', '--test', help='our test parameter')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = PixelStrip(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')
    print(args.test)

    try:

        x = 0
        max_x = 18
        reverse = 0
        wait_ms = 50
        frame = 0
        ballX = 22
        while True:
            #fireworks...
            boom(strip, frame, palette)
            displayBitmap(strip, 0, 0, 32, 16, background, palette)
            ballX = getBallX(frame, ballX)
            displayBitmap(strip, ballX, getBallY(frame), 3, 3, ball, palette)
            displayBitmap(strip, getPlayerX(frame), getPlayerY(frame), 5, 9, player[getPlayerFrame(frame)], palette)
            displayBitmap(strip, 4, 0, 1, 10, post, palette)

            strip.show()
            time.sleep(wait_ms / 1000.0)
            frame += 1
            if (frame == 100):
                frame = 0
                ballX = 22

            fastClear(strip)

    except KeyboardInterrupt:
        if args.clear:
           fastWipe(strip, Color(0, 0, 0))


